function redirectToPage() {
    var select = document.getElementById("language-select");
    var selectedValue = select.value;
    
    // Перенаправлення на обрану сторінку
    window.location.href = selectedValue;
}

var select = document.getElementById("language-select")
select.addEventListener("change", redirectToPage);
